<?php

namespace app\controllers;

use Yii;
use app\models\Builds;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;


/**
 * BuildsController implements the CRUD actions for Builds model.
 */
class BuildsController extends Controller
{
    public static function tableName()
    {
        return 'builds';
    }

    // Define las reglas de validación
    public function rules()
    {
        return [
            [['nombre', 'idpersonaje', 'idcascos', 'idcollar', 'idarma', 'idarmadura', 'idbotas', 'idanillo', 'idvelbotas'], 'required'],
            [['descripcion'], 'string'],
            [['fcreacion'], 'safe'],
            [['efectividad', 'complejidad', 'idpersonaje', 'idcascos', 'idcollar', 'idarma', 'idarmadura', 'idbotas', 'idanillo', 'idvelbotas'], 'integer'],
            [['nombre'], 'string', 'max' => 50],
            [['nombre'], 'unique'],
            [['idpersonaje', 'idcascos', 'idcollar', 'idarma', 'idarmadura', 'idbotas'], 'unique', 'targetAttribute' => ['idpersonaje', 'idcascos', 'idcollar', 'idarma', 'idarmadura', 'idbotas']],
        ];
    }

    // Define etiquetas para los atributos
    public function attributeLabels()
    {
        return [
            'nombre' => 'Nombre',
            'descripcion' => 'Descripción',
            'fcreacion' => 'Fecha de Creación',
            'efectividad' => 'Efectividad',
            'complejidad' => 'Complejidad',
            'idpersonaje' => 'Personaje',
            'idcascos' => 'Casco',
            'idcollar' => 'Collar',
            'idarma' => 'Arma',
            'idarmadura' => 'Armadura',
            'idbotas' => 'Botas',
            'idanillo' => 'Anillo',
            'idvelbotas' => 'Velocidad de Botas',
        ];
    }

    // Define las relaciones
    public function getPersonaje()
    {
        return $this->hasOne(Personajes::class, ['idpersonaje' => 'idpersonaje']);
    }

    public function getCasco()
    {
        return $this->hasOne(Cascos::class, ['idcasco' => 'idcascos']);
    }

    public function getCollar()
    {
        return $this->hasOne(Collares::class, ['idcollar' => 'idcollar']);
    }

    public function getArma()
    {
        return $this->hasOne(Armas::class, ['idarma' => 'idarma']);
    }

    public function getArmadura()
    {
        return $this->hasOne(Armaduras::class, ['idarmadura' => 'idarmadura']);
    }

    public function getBotas()
    {
        return $this->hasOne(Botas::class, ['idbotas' => 'idbotas']);
    }

    public function getAnillo()
    {
        return $this->hasOne(Anillos::class, ['idanillo' => 'idanillo']);
    }

    public function getVelbotas()
    {
        return $this->hasOne(Velocidadbotas::class, ['idbotas' => 'idvelbotas']);
    }
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }
    public function actionCompare()
    {
        $build1 = null;
        $build2 = null;
        $statistics = [];

        if (Yii::$app->request->post()) {
            $id1 = Yii::$app->request->post('build1');
            $id2 = Yii::$app->request->post('build2');

            if ($id1 && $id2) {
                $build1 = Builds::findOne(['nombre' => $id1]);
                $build2 = Builds::findOne(['nombre' => $id2]);

                if (!$build1 || !$build2) {
                    throw new NotFoundHttpException('Una o ambas builds no fueron encontradas.');
                }

                // Estadísticas específicas para la comparación
                $statistics = [
                    'Armadura' => [
                        'valordef' => [$build1->idarmadura0->valordef ?? 'N/A', $build2->idarmadura0->valordef ?? 'N/A'],
                        'porcentfisicodef' => [$build1->idarmadura0->porcentfisicodef ?? 'N/A', $build2->idarmadura0->porcentfisicodef ?? 'N/A'],
                        'porcentelementaldef' => [$build1->idarmadura0->porcentelementaldef ?? 'N/A', $build2->idarmadura0->porcentelementaldef ?? 'N/A'],
                    ],
                    'Anillo' => [
                        'probcrit' => [$build1->idanillo0->probcrit ?? 'N/A', $build2->idanillo0->probcrit ?? 'N/A'],
                    ],
                    'Cascos' => [
                        'vidaextra' => [$build1->idcascos0->vidaextra ?? 'N/A', $build2->idcascos0->vidaextra ?? 'N/A'],
                        'regen' => [$build1->idcascos0->regen ?? 'N/A', $build2->idcascos0->regen ?? 'N/A'],
                    ],
                    'Botas' => [
                        'porcentevasion' => [$build1->idbotas0->porcentevasion ?? 'N/A', $build2->idbotas0->porcentevasion ?? 'N/A'],
                    ],
                    'Arma' => [
                        'valoratk' => [$build1->idarma0->valoratk ?? 'N/A', $build2->idarma0->valoratk ?? 'N/A'],
                        'porcentfisicoatk' => [$build1->idarma0->porcentfisicoatk ?? 'N/A', $build2->idarma0->porcentfisicoatk ?? 'N/A'],
                        'porcentelementalatk' => [$build1->idarma0->porcentelementalatk ?? 'N/A', $build2->idarma0->porcentelementalatk ?? 'N/A'],
                    ],
                    'Velocidad Botas' => [
                        'velmovimiento' => [$build1->idbotas1->velmovimiento ?? 'N/A', $build2->idbotas1->velmovimiento ?? 'N/A'],
                    ],
                ];
            }
        }

        return $this->render('compare', [
            'build1' => $build1,
            'build2' => $build2,
            'statistics' => $statistics,
        ]);
    }




    /**
     * Lists all Builds models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Builds::find(),
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }
    public function actionBuscar()
    {
        // Consulta SQL para obtener los datos requeridos
        $sqlTodasBuilds = '
        SELECT 
            b.nombre AS nombre,
            b.complejidad AS complejidad,
            b.efectividad AS efectividad,
            p.nombreesp AS personaje_nombre
        FROM 
            builds b
        LEFT JOIN 
            personajes p ON b.idpersonaje = p.idpersonaje
        ';

        // Configuración para el SqlDataProvider de 'todasBuilds'
        $dataProviderTodasBuilds = new SqlDataProvider([
            'sql' => $sqlTodasBuilds,
        ]);

        // Consulta SQL para obtener las tres builds más recientes
        $sqlTresRecientes = '
        SELECT 
            b.nombre AS nombre,
            b.fcreacion AS fecha_creacion,
            b.efectividad AS efectividad,
            b.complejidad AS complejidad,
            p.nombreesp AS personaje_nombre
        FROM 
            builds b
        LEFT JOIN 
            personajes p ON b.idpersonaje = p.idpersonaje
        ORDER BY 
            b.fcreacion DESC
        LIMIT 3
        ';

        // Configuración para el SqlDataProvider de 'tresRecientes'
        $dataProviderTresRecientes = new SqlDataProvider([
            'sql' => $sqlTresRecientes,
            'pagination' => false, // Desactiva la paginación
            'sort' => false,       // Desactiva el ordenamiento
        ]);

        // Renderiza la vista 'buscar' con los datos proporcionados
        return $this->render('buscar', [
            'todasBuilds' => $dataProviderTodasBuilds,
            'tresRecientes' => $dataProviderTresRecientes,
        ]);
    }
    public function actionVer($nombre)
    {
        // Consulta SQL para obtener los datos de la build específica
        $sqlBuild = '
        SELECT 
            b.nombre AS nombre,
            b.descripcion AS descripcion,
            b.fcreacion AS fecha_creacion,
            b.efectividad AS efectividad,
            b.complejidad AS complejidad,
            p.nombreesp AS personaje_nombre,
            c.nombre AS casco_nombre,
            c.vidaextra AS casco_vidaextra,
            c.regen AS casco_regen,
            col.nombre AS collar_nombre,
            col.efectoesp AS collar_efectoesp,
            a.nombre AS arma_nombre,
            a.valoratk AS arma_valoratk,
            a.porcentfisicoatk AS arma_porcentfisicoatk,
            a.porcentelementalatk AS arma_porcentelementalatk,
            arm.nombre AS armadura_nombre,
            arm.valordef AS armadura_valordef,
            arm.porcentfisicodef AS armadura_porcentfisicodef,
            arm.porcentelementaldef AS armadura_porcentelementaldef,
            bot.nombre AS botas_nombre,
            bot.porcentevasion AS botas_porcentevasion,
            an.nombre AS anillo_nombre,
            an.probcrit AS anillo_probcrit,
            vb.velmovimiento AS velocidad_botas
        FROM 
            builds b
        LEFT JOIN 
            personajes p ON b.idpersonaje = p.idpersonaje
        LEFT JOIN 
            cascos c ON b.idcascos = c.idcasco
        LEFT JOIN 
            collares col ON b.idcollar = col.idcollar
        LEFT JOIN 
            armas a ON b.idarma = a.idarma
        LEFT JOIN 
            armaduras arm ON b.idarmadura = arm.idarmadura
        LEFT JOIN 
            botas bot ON b.idbotas = bot.idbotas
        LEFT JOIN 
            anillos an ON b.idanillo = an.idanillo
        LEFT JOIN 
            velocidadbotas vb ON b.idvelbotas = vb.idbotas
        WHERE 
            b.nombre = :nombre
        ';

        // Ejecutar la consulta
        $build = Yii::$app->db->createCommand($sqlBuild, [':nombre' => $nombre])->queryOne();

        // Renderizar la vista 'ver' con los datos de la build
        return $this->render('ver', [
            'build' => $build,
        ]);
    }

    public function actionExplorar()
    {
    return $this->render('explorar', [
        'puerto2' => new SqlDataProvider([
                "sql" => 'SELECT DISTINCT nombre AS nombre FROM builds',
            ]),
            'campos2' => ['nombre'],
        ]);
    }
    public function actionEquipo()
    {
    return $this->render('equipo', [
        'puerto2' => new SqlDataProvider([
                "sql" => 'SELECT DISTINCT nombre AS nombre FROM builds',
            ]),
            'campos2' => ['nombre'],
        ]);
    }
    public function actionFacil()
{
    // Consulta SQL para obtener las builds con complejidad menor o igual que 3
    $sql = '
        SELECT 
            COALESCE(b.nombre, "N/A") AS nombre,
            COALESCE(b.descripcion, "N/A") AS descripcion,
            COALESCE(b.fcreacion, "N/A") AS fecha_creacion,
            COALESCE(b.efectividad, "N/A") AS efectividad,
            COALESCE(b.complejidad, "N/A") AS complejidad,
            COALESCE(p.nombreesp, "N/A") AS personaje_nombre,
            COALESCE(c.nombre, "N/A") AS casco_nombre,
            COALESCE(col.nombre, "N/A") AS collar_nombre,
            COALESCE(a.nombre, "N/A") AS arma_nombre,
            COALESCE(ar.nombre, "N/A") AS armadura_nombre,
            COALESCE(bo.nombre, "N/A") AS botas_nombre,
            COALESCE(an.nombre, "N/A") AS anillo_nombre
        FROM 
            builds b
        LEFT JOIN 
            personajes p ON b.idpersonaje = p.idpersonaje
        LEFT JOIN 
            cascos c ON b.idcascos = c.idcasco
        LEFT JOIN 
            collares col ON b.idcollar = col.idcollar
        LEFT JOIN 
            armas a ON b.idarma = a.idarma
        LEFT JOIN 
            armaduras ar ON b.idarmadura = ar.idarmadura
        LEFT JOIN 
            botas bo ON b.idbotas = bo.idbotas
        LEFT JOIN 
            anillos an ON b.idanillo = an.idanillo
        WHERE 
            b.complejidad <= 3 OR b.complejidad IS NULL
    ';

    return $this->render('facil', [
        'faciles' => new SqlDataProvider([
            'sql' => $sql,
        ]),
    ]);
}
public function actionMedio()
{
    // Consulta SQL para obtener las builds con complejidad entre 4 y 7
    $sql = '
        SELECT 
            COALESCE(b.nombre, "N/A") AS nombre,
            COALESCE(b.descripcion, "N/A") AS descripcion,
            COALESCE(b.fcreacion, "N/A") AS fecha_creacion,
            COALESCE(b.efectividad, "N/A") AS efectividad,
            COALESCE(b.complejidad, "N/A") AS complejidad,
            COALESCE(p.nombreesp, "N/A") AS personaje_nombre,
            COALESCE(c.nombre, "N/A") AS casco_nombre,
            COALESCE(col.nombre, "N/A") AS collar_nombre,
            COALESCE(a.nombre, "N/A") AS arma_nombre,
            COALESCE(ar.nombre, "N/A") AS armadura_nombre,
            COALESCE(bo.nombre, "N/A") AS botas_nombre,
            COALESCE(an.nombre, "N/A") AS anillo_nombre
        FROM 
            builds b
        LEFT JOIN 
            personajes p ON b.idpersonaje = p.idpersonaje
        LEFT JOIN 
            cascos c ON b.idcascos = c.idcasco
        LEFT JOIN 
            collares col ON b.idcollar = col.idcollar
        LEFT JOIN 
            armas a ON b.idarma = a.idarma
        LEFT JOIN 
            armaduras ar ON b.idarmadura = ar.idarmadura
        LEFT JOIN 
            botas bo ON b.idbotas = bo.idbotas
        LEFT JOIN 
            anillos an ON b.idanillo = an.idanillo
        WHERE 
            b.complejidad BETWEEN 4 AND 7 OR b.complejidad IS NULL
    ';
    
    return $this->render('medio', [
        'faciles' => new SqlDataProvider([
            'sql' => $sql,
        ]),
    ]);
}
public function actionDificil()
{
    // Consulta SQL para obtener las builds con complejidad mayor que 7
    $sql = '
        SELECT 
            COALESCE(b.nombre, "N/A") AS nombre,
            COALESCE(b.descripcion, "N/A") AS descripcion,
            COALESCE(b.fcreacion, "N/A") AS fecha_creacion,
            COALESCE(b.efectividad, "N/A") AS efectividad,
            COALESCE(b.complejidad, "N/A") AS complejidad,
            COALESCE(p.nombreesp, "N/A") AS personaje_nombre,
            COALESCE(c.nombre, "N/A") AS casco_nombre,
            COALESCE(col.nombre, "N/A") AS collar_nombre,
            COALESCE(a.nombre, "N/A") AS arma_nombre,
            COALESCE(ar.nombre, "N/A") AS armadura_nombre,
            COALESCE(bo.nombre, "N/A") AS botas_nombre,
            COALESCE(an.nombre, "N/A") AS anillo_nombre
        FROM 
            builds b
        LEFT JOIN 
            personajes p ON b.idpersonaje = p.idpersonaje
        LEFT JOIN 
            cascos c ON b.idcascos = c.idcasco
        LEFT JOIN 
            collares col ON b.idcollar = col.idcollar
        LEFT JOIN 
            armas a ON b.idarma = a.idarma
        LEFT JOIN 
            armaduras ar ON b.idarmadura = ar.idarmadura
        LEFT JOIN 
            botas bo ON b.idbotas = bo.idbotas
        LEFT JOIN 
            anillos an ON b.idanillo = an.idanillo
        WHERE 
            b.complejidad > 7 OR b.complejidad IS NULL
    ';
    
    return $this->render('dificil', [
        'faciles' => new SqlDataProvider([
            'sql' => $sql,
        ]),
    ]);
}
public function actionMapas()
{
    // Consulta SQL para obtener las columnas específicas de las builds que cumplen con los criterios
    $sql = '
        SELECT 
            b.nombre,
            b.descripcion,
            b.fcreacion,
            b.efectividad,
            b.complejidad,
            p.nombreesp AS personaje_nombre,
            c.nombre AS casco_nombre,
            col.nombre AS collar_nombre,
            a.nombre AS arma_nombre,
            ar.nombre AS armadura_nombre,
            bo.nombre AS botas_nombre,
            an.nombre AS anillo_nombre,
            a.valoratk,
            a.porcentfisicoatk,
            a.porcentelementalatk
        FROM 
            builds b
        LEFT JOIN 
            personajes p ON b.idpersonaje = p.idpersonaje
        LEFT JOIN 
            cascos c ON b.idcascos = c.idcasco
        LEFT JOIN 
            collares col ON b.idcollar = col.idcollar
        LEFT JOIN 
            armas a ON b.idarma = a.idarma
        LEFT JOIN 
            armaduras ar ON b.idarmadura = ar.idarmadura
        LEFT JOIN 
            botas bo ON b.idbotas = bo.idbotas
        LEFT JOIN 
            anillos an ON b.idanillo = an.idanillo
        WHERE 
            a.valoratk > 250 OR a.porcentfisicoatk > 35 OR a.porcentelementalatk > 35
    ';
    
    return $this->render('mapas', [
        'filtroMapas' => new SqlDataProvider([
            'sql' => $sql,
        ]),
    ]);
}
public function actionJefes()
{
    // Consulta SQL para obtener las columnas específicas de las builds que cumplen con los nuevos criterios
    $sql = '
        SELECT 
            b.nombre,
            b.descripcion,
            b.fcreacion,
            b.efectividad,
            b.complejidad,
            p.nombreesp AS personaje_nombre,
            c.nombre AS casco_nombre,
            col.nombre AS collar_nombre,
            a.nombre AS arma_nombre,
            ar.nombre AS armadura_nombre,
            bo.nombre AS botas_nombre,
            an.nombre AS anillo_nombre,
            a.valoratk,
            a.porcentfisicoatk,
            a.porcentelementalatk,
            ar.valordef,
            ar.porcentfisicodef,
            ar.porcentelementaldef
        FROM 
            builds b
        LEFT JOIN 
            personajes p ON b.idpersonaje = p.idpersonaje
        LEFT JOIN 
            cascos c ON b.idcascos = c.idcasco
        LEFT JOIN 
            collares col ON b.idcollar = col.idcollar
        LEFT JOIN 
            armas a ON b.idarma = a.idarma
        LEFT JOIN 
            armaduras ar ON b.idarmadura = ar.idarmadura
        LEFT JOIN 
            botas bo ON b.idbotas = bo.idbotas
        LEFT JOIN 
            anillos an ON b.idanillo = an.idanillo
        WHERE 
            ar.valordef > 150
            AND ar.porcentfisicodef > 15
            AND ar.porcentelementaldef > 15
    ';
    
    return $this->render('jefes', [
        'filtroMapas' => new SqlDataProvider([
            'sql' => $sql,
        ]),
    ]);
}

public function actionBajaefec()
{
    // Consulta SQL para obtener todas las columnas de las builds cuya efectividad sea menor que 3
    $sql = '
        SELECT 
            b.nombre,
            b.descripcion,
            b.fcreacion,
            b.efectividad,
            b.complejidad,
            p.nombreesp AS personaje_nombre,
            c.nombre AS casco_nombre,
            col.nombre AS collar_nombre,
            a.nombre AS arma_nombre,
            ar.nombre AS armadura_nombre,
            bo.nombre AS botas_nombre,
            an.nombre AS anillo_nombre
        FROM 
            builds b
        LEFT JOIN 
            personajes p ON b.idpersonaje = p.idpersonaje
        LEFT JOIN 
            cascos c ON b.idcascos = c.idcasco
        LEFT JOIN 
            collares col ON b.idcollar = col.idcollar
        LEFT JOIN 
            armas a ON b.idarma = a.idarma
        LEFT JOIN 
            armaduras ar ON b.idarmadura = ar.idarmadura
        LEFT JOIN 
            botas bo ON b.idbotas = bo.idbotas
        LEFT JOIN 
            anillos an ON b.idanillo = an.idanillo
        WHERE 
            b.efectividad <= 3
    ';
    
    return $this->render('bajaefec', [
        'pocoEfectivas' => new SqlDataProvider([
            'sql' => $sql,
        ]),
    ]);
}
public function actionMediaefec()
{
    // Consulta SQL para obtener todas las columnas de las builds cuya efectividad sea entre 4 y 7
    $sql = '
        SELECT 
            b.nombre,
            b.descripcion,
            b.fcreacion,
            b.efectividad,
            b.complejidad,
            p.nombreesp AS personaje_nombre,
            c.nombre AS casco_nombre,
            col.nombre AS collar_nombre,
            a.nombre AS arma_nombre,
            ar.nombre AS armadura_nombre,
            bo.nombre AS botas_nombre,
            an.nombre AS anillo_nombre
        FROM 
            builds b
        LEFT JOIN 
            personajes p ON b.idpersonaje = p.idpersonaje
        LEFT JOIN 
            cascos c ON b.idcascos = c.idcasco
        LEFT JOIN 
            collares col ON b.idcollar = col.idcollar
        LEFT JOIN 
            armas a ON b.idarma = a.idarma
        LEFT JOIN 
            armaduras ar ON b.idarmadura = ar.idarmadura
        LEFT JOIN 
            botas bo ON b.idbotas = bo.idbotas
        LEFT JOIN 
            anillos an ON b.idanillo = an.idanillo
        WHERE 
            b.efectividad BETWEEN 4 AND 7
    ';
    
    return $this->render('mediaefec', [
        'medioEfectivas' => new SqlDataProvider([
            'sql' => $sql,
        ]),
    ]);
}
public function actionAltaefec()
{
    // Consulta SQL para obtener todas las columnas de las builds cuya efectividad sea mayor que 7
    $sql = '
        SELECT 
            b.nombre,
            b.descripcion,
            b.fcreacion,
            b.efectividad,
            b.complejidad,
            p.nombreesp AS personaje_nombre,
            c.nombre AS casco_nombre,
            col.nombre AS collar_nombre,
            a.nombre AS arma_nombre,
            ar.nombre AS armadura_nombre,
            bo.nombre AS botas_nombre,
            an.nombre AS anillo_nombre
        FROM 
            builds b
        LEFT JOIN 
            personajes p ON b.idpersonaje = p.idpersonaje
        LEFT JOIN 
            cascos c ON b.idcascos = c.idcasco
        LEFT JOIN 
            collares col ON b.idcollar = col.idcollar
        LEFT JOIN 
            armas a ON b.idarma = a.idarma
        LEFT JOIN 
            armaduras ar ON b.idarmadura = ar.idarmadura
        LEFT JOIN 
            botas bo ON b.idbotas = bo.idbotas
        LEFT JOIN 
            anillos an ON b.idanillo = an.idanillo
        WHERE 
            b.efectividad > 7
    ';
    
    return $this->render('altaefec', [
        'altaEfectivas' => new SqlDataProvider([
            'sql' => $sql,
        ]),
    ]);
}
    
    

    /**
     * Displays a single Builds model.
     * @param string $nombre Nombre
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($nombre)
    {
        return $this->render('view', [
            'model' => $this->findModel($nombre),
        ]);
    }

    /**
     * Creates a new Builds model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Builds();

        if ($this->request->isPost) {
            if ($model->load($this->request->post())) {
                // No establecer fecha_creacion aquí, se hará en el modelo
                if ($model->save()) {
                    return $this->redirect(['view', 'id' => $model->id]);
                }
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Builds model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $nombre Nombre
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($nombre)
    {
        $model = $this->findModel($nombre);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'nombre' => $model->nombre]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Builds model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $nombre Nombre
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($nombre)
    {
        $this->findModel($nombre)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Builds model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $nombre Nombre
     * @return Builds the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($nombre)
    {
        if (($model = Builds::findOne(['nombre' => $nombre])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    
    
}
