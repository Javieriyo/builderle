<?php

/** @var yii\web\View $this */
/** @var string $content */

use app\assets\AppAsset;
use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap4\Breadcrumbs;
use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="h-100">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <link rel="icon" type="image/png" href="<?= Yii::getAlias('@web') ?>/images/logoNav.jpg"> <!-- Enlace al icono -->
    <?php $this->head() ?>
</head>
<body class="d-flex flex-column h-100">
<?php $this->beginBody() ?>

<header>
<div class="encabezado">
    <?= Html::a('CREAR', ['builds/create'], ['class' => 'boton-encabezado']) ?>
    <?= Html::a('BUSCAR', ['builds/explorar'], ['class' => 'boton-encabezado']) ?>
    <div class="logo-encabezado-container">
        <?= Html::a(Html::img('@web/images/logo.png', ['class' => 'logo']), ['site/index'], ['class' => 'logo-link']) ?>
    </div>
    <?= Html::a('EXPLORAR', ['builds/buscar'], ['class' => 'boton-encabezado']) ?>
    <?= Html::a('EQUIPO', ['builds/equipo'], ['class' => 'boton-encabezado']) ?>
</div>
</header>

<main role="main" class="flex-shrink-0">
    <div class="container">
        
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</main>

<footer class="footer mt-auto py-3 text-muted">
    <div class="container">
        <p class="float-left">&copy; BuilderLE <?= date('Y') ?></p>
        <p class="float-right">Desarrollado por Javier Campocosio Regato</p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
