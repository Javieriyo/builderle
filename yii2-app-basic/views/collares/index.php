<?php

use app\models\Collares;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Collares';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="collares-index">

<div class="contenedor-titulo">
    <h1 class="titulo">COLLARES</h1>
</div>

<div class="contenedor-tabla-tresrecientes">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            // Eliminar la columna de número de fila
            // ['class' => 'yii\grid\SerialColumn'],

            'idcollar',
            'nombre',
            'efectoesp',
            // Eliminar la columna de acciones
            // [
            //     'class' => ActionColumn::className(),
            //     'urlCreator' => function ($action, Collares $model, $key, $index, $column) {
            //         return Url::toRoute([$action, 'idcollar' => $model->idcollar]);
            //      }
            // ],
        ],
        'tableOptions' => ['class' => 'tabla-tresRecientes'],
        'summary' => '',
    ]); ?>
</div>
<div class="contenedor-titulo">
    <?= Html::a('VOLVER AL EQUIPO', ['builds/equipo'], ['class' => 'boton-vuelta-a-explorar']) ?>
</div>
</div>
