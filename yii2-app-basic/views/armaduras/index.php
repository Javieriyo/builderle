<?php

use app\models\Armaduras;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Armaduras';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="armaduras-index">

<div class="contenedor-titulo">
    <h1 class="titulo">ARMADURAS</h1>
</div>

<div class="contenedor-tabla-tresrecientes">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            // Eliminar la columna de número de fila
            // ['class' => 'yii\grid\SerialColumn'],

            'idarmadura',
            'nombre',
            'valordef',
            'porcentfisicodef',
            'porcentelementaldef',
            // Eliminar la columna de acciones
            // [
            //     'class' => ActionColumn::className(),
            //     'urlCreator' => function ($action, Armaduras $model, $key, $index, $column) {
            //         return Url::toRoute([$action, 'idarmadura' => $model->idarmadura]);
            //      }
            // ],
        ],
        'tableOptions' => ['class' => 'tabla-tresRecientes'],
        'summary' => '',
    ]); ?>

</div>
<div class="contenedor-titulo">
    <?= Html::a('VOLVER AL EQUIPO', ['builds/equipo'], ['class' => 'boton-vuelta-a-explorar']) ?>
</div>
</div>
