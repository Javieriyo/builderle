<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Builds|null $build1 */
/** @var app\models\Builds|null $build2 */
/** @var array $statistics */

$this->title = 'Comparar Builds';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="contenedor-titulo">
    <h1 class="titulo">COMPARAR BUILDS</h1>
</div>
<hr class="linea-divisoria-central">
<div class="builds-compare">

    <div class="build-selection">
        <div class="centrador">
            <?php $form = ActiveForm::begin([
                'action' => Url::to(['builds/compare']),
                'method' => 'post',
            ]); ?>
        </div>

        <?= Html::dropDownList('build1', null, \yii\helpers\ArrayHelper::map(app\models\Builds::find()->all(), 'nombre', 'nombre'), [
            'prompt' => 'Selecciona la primera build',
            'class' => 'build-selector' // Agrega una clase personalizada para el selector de builds
        ]) ?>
        <?= Html::dropDownList('build2', null, \yii\helpers\ArrayHelper::map(app\models\Builds::find()->all(), 'nombre', 'nombre'), [
            'prompt' => 'Selecciona la segunda build',
            'class' => 'build-selector' // Agrega una clase personalizada para el selector de builds
        ]) ?>
        <div class="form-group">
            <?= Html::submitButton('Comparar', ['class' => 'boton-encabezado']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>

    <?php if ($build1 && $build2): ?>
        <div class="contenedor-tabla-tresrecientes">
            <div class="tabla-tresRecientes">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Estadística</th>
                            <th><?= Html::encode($build1->nombre) ?></th>
                            <th><?= Html::encode($build2->nombre) ?></th>
                            <th>Diferencia</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($statistics as $category => $stats): ?>
                            <?php foreach ($stats as $statName => $values): ?>
                                <tr>
                                    <td><?= Html::encode($statName) ?></td>
                                    <td><?= Html::encode($values[0]) ?></td>
                                    <td><?= Html::encode($values[1]) ?></td>
                                    <td>
                                        <?php
                                            $value1 = is_numeric($values[0]) ? $values[0] : 'N/A';
                                            $value2 = is_numeric($values[1]) ? $values[1] : 'N/A';
                                            $difference = is_numeric($value1) && is_numeric($value2) ? $value1 - $value2 : 'N/A';
                                            echo Html::encode($difference);
                                        ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    <?php endif; ?>

</div>
