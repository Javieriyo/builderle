<?php

use app\models\Builds;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\web\Controller;
use yii\data\SqlDataProvider;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Builds';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="contenedor-titulo">
    <h1 class="titulo">BUSCA LA BUILD QUE MAS TE CONVENGA</h1>
</div>
<hr class="linea-divisoria-derecha">
<!--FILTRADO POR DIFICULTAD-->
<div class="contenedor-titulo">
    <h1 class="subtitulo">FILTRADO POR DIFICULTAD</h1>
</div>
<div class="contenedor-boton-filtrado">
    <?= Html::a('FACIL', ['builds/facil'], ['class' => 'boton-filtrado-dificultad-facil']) ?>
    <?= Html::a('MEDIO', ['builds/medio'], ['class' => 'boton-filtrado-dificultad-medio']) ?>
    <?= Html::a('DIFICIL', ['builds/dificil'], ['class' => 'boton-filtrado-dificultad-dificil']) ?>
</div>
<br>
<hr class="linea-divisoria-izquierda">

<!--FILTRADO POR CONTENIDO-->

<div class="contenedor-titulo">
    <h1 class="subtitulo">FILTRADO POR CONTENIDO</h1>
</div>
<div class="contenedor-boton-filtrado">
    <?= Html::a('MAPAS', ['builds/mapas'], ['class' => 'boton-filtrado-contenido-mapas']) ?>
    <?= Html::a('JEFES', ['builds/jefes'], ['class' => 'boton-filtrado-contenido-jefes']) ?>
</div>
<br>
<hr class="linea-divisoria-derecha">

<!--FILTRADO POR EFECTIVIDAD-->

<div class="contenedor-titulo">
    <h1 class="subtitulo" style="margin-bottom: -10px">FILTRADO POR EFECTIVIDAD GENERAL</h1>
</div>
<div class="contenedor-titulo">
    <h1 class="subtitulo" style="font-size:20px">(Mayor efectividad requerirá un mayor presupuesto)</h1>
</div>
<div class="contenedor-boton-filtrado">
    <?= Html::a('BAJA', ['builds/bajaefec'], ['class' => 'boton-filtrado-efectividad-baja']) ?>
    <?= Html::a('MEDIA', ['builds/mediaefec'], ['class' => 'boton-filtrado-efectividad-media']) ?>
    <?= Html::a('ALTA', ['builds/altaefec'], ['class' => 'boton-filtrado-efectividad-alta']) ?>
</div>
<hr class="linea-divisoria-izquierda">