<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Builds $model */
/** @var yii\widgets\ActiveForm $form */
?>
<div class="contenedor-titulo">
    <h1 class="titulo">COMIENZA A CREAR</h1>
</div>
<br class="linea-divisoria-central">
<hr class="linea-divisoria-central">

<?php $form = ActiveForm::begin(['options' => ['class' => 'form-horizontal']]); ?>

<?= $form->field($model, 'nombre', ['options' => ['class' => 'form-group']])->textInput(['maxlength' => true, 'class' => 'form-control']) ?>
<?= $form->field($model, 'descripcion', ['options' => ['class' => 'form-group']])->textarea(['rows' => 6, 'class' => 'form-control']) ?>
<?= $form->field($model, 'complejidad', ['options' => ['class' => 'form-group']])->textInput(['type' => 'number', 'class' => 'form-control']) ?>
<?= $form->field($model, 'efectividad', ['options' => ['class' => 'form-group']])->textInput(['type' => 'number', 'class' => 'form-control']) ?>
<?= $form->field($model, 'idpersonaje', ['options' => ['class' => 'form-group']])->textInput(['class' => 'form-control']) ?>
<?= $form->field($model, 'idcascos', ['options' => ['class' => 'form-group']])->textInput(['class' => 'form-control']) ?>
<?= $form->field($model, 'idcollar', ['options' => ['class' => 'form-group']])->textInput(['class' => 'form-control']) ?>
<?= $form->field($model, 'idarma', ['options' => ['class' => 'form-group']])->textInput(['class' => 'form-control']) ?>
<?= $form->field($model, 'idarmadura', ['options' => ['class' => 'form-group']])->textInput(['class' => 'form-control']) ?>
<?= $form->field($model, 'idbotas', ['options' => ['class' => 'form-group']])->textInput(['class' => 'form-control']) ?>
<?= $form->field($model, 'idanillo', ['options' => ['class' => 'form-group']])->textInput(['class' => 'form-control']) ?>

<div class="contenedor-titulo">
    <?= Html::submitButton('Save', ['class' => 'boton-encabezado']) ?>
</div>

<?php ActiveForm::end(); ?>

</div>
