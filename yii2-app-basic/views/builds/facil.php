<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\data\SqlDataProvider;

/** @var yii\web\View $this */
/** @var yii\data\SqlDataProvider $faciles */

$this->title = 'Buscar';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contenedor-titulo">
    <h1 class="titulo">BUILDS FÁCILES DE JUGAR</h1>
</div>

<div class="contenedor-tabla-tresrecientes">
    <?= GridView::widget([
        'dataProvider' => $faciles,
        'columns' => [
            [
                'attribute' => 'nombre',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a(Html::encode($model['nombre']), ['builds/ver', 'nombre' => $model['nombre']]);
                },
            ],
            'efectividad',
            'complejidad',
            'personaje_nombre',
            'casco_nombre',
            'collar_nombre',
            'arma_nombre',
            'armadura_nombre',
            'botas_nombre',
            'anillo_nombre',
        ],
        'tableOptions' => ['class' => 'tabla-tresRecientes'],
        'summary' => '', // Oculta el resumen
    ]); ?>
</div>

<!-- Botón de vuelta -->
<div class="contenedor-titulo">
    <?= Html::a('VOLVER A EXPLORAR', ['builds/explorar'], ['class' => 'boton-vuelta-a-explorar']) ?>
</div>
