<?php

use app\models\Builds;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\web\Controller;
use yii\data\SqlDataProvider;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Builds';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="contenedor-titulo">
    <h1 class="titulo">EXPLORA TODO EL EQUIPAMIENTO DISPONIBLE</h1>
</div>
<hr class="linea-divisoria-derecha">

<div class="contenedor-boton-filtrado">
    <a href="<?= Yii::$app->urlManager->createUrl(['cascos/index']) ?>" class="boton-filtrado-contenido-cascos">CASCOS</a>
    <a href="<?= Yii::$app->urlManager->createUrl(['anillo/index']) ?>" class="boton-filtrado-contenido-anillos" class="anillos">ANILLOS</a>
</div>
<div class="contenedor-boton-filtrado">
    <a href="<?= Yii::$app->urlManager->createUrl(['armaduras/index']) ?>" class="boton-filtrado-contenido-armaduras">ARMADURAS</a>
    <a href="<?= Yii::$app->urlManager->createUrl(['armas/index']) ?>" class="boton-filtrado-contenido-armas">ARMAS</a>
</div>
<div class="contenedor-boton-filtrado">
    <a href="<?= Yii::$app->urlManager->createUrl(['collares/index']) ?>" class="boton-filtrado-contenido-collares">COLLARES</a>
    <a href="<?= Yii::$app->urlManager->createUrl(['botas/index']) ?>" class="boton-filtrado-contenido-botas">BOTAS</a>
</div>
