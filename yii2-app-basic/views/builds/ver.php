<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var array $build */

$this->title = 'Detalles de la Build: ' . Html::encode($build['nombre']);
$this->params['breadcrumbs'][] = ['label' => 'Buscar', 'url' => ['buscar']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contenedor-titulo">
    <h1 class="titulo"><?= Html::encode($build['nombre']) ?></h1>
</div>
<br>
    <div class="contenedor-titulo">
        <h1 class="subtitulo">DETALLES GENERALES</h1>
    </div>
    <hr class="linea-divisoria-central">
    <div class="contenedor-titulo">
        <h1 class="minisubtitulo"><strong>fecha de creacion: <?= Html::encode($build['fecha_creacion']) ?></h1>
    </div>
    <div class="contenedor-titulo">
        <h1 class="minisubtitulo"><?= Html::encode($build['descripcion']) ?></h1>
    </div>
    <div class="contenedor-dificultad-complejidad">
        <div class="contenedor-efectividad">
            <p class="minisubtitulo"><strong>Efectividad:</strong> <?= Html::encode($build['efectividad']) ?>/10</p>
        </div>
        <div class="contenedor-complejidad">
            <p class="minisubtitulo"><strong>Complejidad:</strong> <?= Html::encode($build['complejidad']) ?>/10</p>
        </div>
    </div>
    <div class="contenedor-titulo">
        <p class="minisubtitulo"><strong>Nombre de la especialización:</strong> <?= Html::encode($build['personaje_nombre']) ?></p>
    </div>
    <br>
    <div class="contenedor-titulo">
        <h1 class="subtitulo">EQUIPAMIENTO</h1>
    </div>
    <hr class="linea-divisoria-central">
    
    <div class="contenedor-equipamiento">
        <div class="contenedor-equipamiento-individual">
            <p class="minisubtitulo"><strong>Casco:</strong> <?= Html::encode($build['casco_nombre']) ?></p>
        </div>
        <div class="contenedor-equipamiento-individual">
            <p class="minisubtitulo"><strong>Collar:</strong> <?= Html::encode($build['collar_nombre']) ?></p>
        </div>
        <div class="contenedor-equipamiento-individual">
            <p class="minisubtitulo"><strong>Arma:</strong> <?= Html::encode($build['arma_nombre']) ?></p>
        </div>
    </div>
    <div class="contenedor-equipamiento">
        <div class="contenedor-equipamiento-individual">
            <p class="minisubtitulo"><strong>Armadura:</strong> <?= Html::encode($build['armadura_nombre']) ?></p>
        </div>
        <div class="contenedor-equipamiento-individual">
            <p class="minisubtitulo"><strong>Botas:</strong> <?= Html::encode($build['botas_nombre']) ?></p>
        </div>
        <div class="contenedor-equipamiento-individual">
            <p class="minisubtitulo"><strong>Anillo:</strong> <?= Html::encode($build['anillo_nombre']) ?></p>
        </div>
    </div>

    <br>
    <div class="contenedor-titulo">
        <h1 class="subtitulo">ESTADISTICAS</h1>
    </div>
    <hr class="linea-divisoria-central">

    <div class="contenedor-equipamiento">
        <div class="contenedor-equipamiento-individual">
            <p class="minisubtitulo"><strong>Vida Extra:</strong> <?= Html::encode($build['casco_vidaextra']) ?> PS</p>
        </div>
        <div class="contenedor-equipamiento-individual">
            <p class="minisubtitulo"><strong>Regeneración:</strong> <?= Html::encode($build['casco_regen']) ?> salud/s</p>
        </div>
        <div class="contenedor-equipamiento-individual">
            <p class="minisubtitulo"><strong>Evasión:</strong> <?= Html::encode($build['botas_porcentevasion']) ?> % de evadir</p>
        </div>
    </div>
    <div class="contenedor-equipamiento">
        <div class="contenedor-equipamiento-individual">
            <p class="minisubtitulo"><strong>Ataque:</strong> <?= Html::encode($build['arma_valoratk']) ?></p>
        </div>
        <div class="contenedor-equipamiento-individual">
            <p class="minisubtitulo"><strong>Atk físico:</strong> <?= Html::encode($build['arma_porcentfisicoatk']) ?>%</p>
        </div>
        <div class="contenedor-equipamiento-individual">
            <p class="minisubtitulo"><strong>Atk elemental:</strong> <?= Html::encode($build['arma_porcentelementalatk']) ?>%</p>
        </div>
    </div>
    <div class="contenedor-equipamiento">
        <div class="contenedor-equipamiento-individual">
            <p class="minisubtitulo"><strong>Armadura:</strong> <?= Html::encode($build['armadura_valordef']) ?></p>
        </div>
        <div class="contenedor-equipamiento-individual">
            <p class="minisubtitulo"><strong>Porcentaje def fisica:</strong> <?= Html::encode($build['armadura_porcentfisicodef']) ?>%</p>
        </div>
        <div class="contenedor-equipamiento-individual">
            <p class="minisubtitulo"><strong>Porcentaje def elemental:</strong> <?= Html::encode($build['armadura_porcentelementaldef']) ?>%</p>
        </div>
    </div>
    <div class="contenedor-equipamiento">
        <div class="contenedor-equipamiento-individual">
            <p class="minisubtitulo"><strong>Probabilidad crítica:</strong> <?= Html::encode($build['anillo_probcrit']) ?>%</p>
        </div>
        <div class="contenedor-equipamiento-individual">
            <p class="minisubtitulo"><strong>Velocidad extra:</strong> <?= Html::encode($build['velocidad_botas']) ?>%</p>
        </div>
    </div>

    <br>
    <div class="contenedor-titulo">
        <h1 class="subtitulo">EFECTO ESPECIAL</h1>
    </div>
    <hr class="linea-divisoria-central">
    <div class="contenedor-titulo">
        <h1 class="minisubtitulo"><?= Html::encode($build['collar_efectoesp']) ?></h1>
    </div>
    <br>
    
