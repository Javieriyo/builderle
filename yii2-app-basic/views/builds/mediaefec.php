<?php

use yii\grid\GridView;
use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var yii\data\SqlDataProvider $medioEfectivas */
/** @var array $campos2 */

$this->title = 'Builds con Efectividad Media';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="contenedor-titulo">
    <h1 class="titulo">BUILDS CON EFECTIVIDAD MEDIA</h1>
</div>

<div class="contenedor-tabla-tresrecientes">
    <?= GridView::widget([
        'dataProvider' => $medioEfectivas,
        'columns' => [
            [
                'attribute' => 'nombre',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a(Html::encode($model['nombre']), ['builds/ver', 'nombre' => $model['nombre']]);
                },
            ],
            [
                'attribute' => 'efectividad',
                'value' => function ($model) {
                    return $model['efectividad'] ?: 'N/A';
                }
            ],
            [
                'attribute' => 'complejidad',
                'value' => function ($model) {
                    return $model['complejidad'] ?: 'N/A';
                }
            ],
            [
                'attribute' => 'personaje_nombre',
                'label' => 'Personaje',
                'value' => function ($model) {
                    return $model['personaje_nombre'] ?: 'N/A';
                }
            ],
            [
                'attribute' => 'casco_nombre',
                'label' => 'Casco',
                'value' => function ($model) {
                    return $model['casco_nombre'] ?: 'N/A';
                }
            ],
            [
                'attribute' => 'collar_nombre',
                'label' => 'Collar',
                'value' => function ($model) {
                    return $model['collar_nombre'] ?: 'N/A';
                }
            ],
            [
                'attribute' => 'arma_nombre',
                'label' => 'Arma',
                'value' => function ($model) {
                    return $model['arma_nombre'] ?: 'N/A';
                }
            ],
            [
                'attribute' => 'armadura_nombre',
                'label' => 'Armadura',
                'value' => function ($model) {
                    return $model['armadura_nombre'] ?: 'N/A';
                }
            ],
            [
                'attribute' => 'botas_nombre',
                'label' => 'Botas',
                'value' => function ($model) {
                    return $model['botas_nombre'] ?: 'N/A';
                }
            ],
            [
                'attribute' => 'anillo_nombre',
                'label' => 'Anillo',
                'value' => function ($model) {
                    return $model['anillo_nombre'] ?: 'N/A';
                }
            ],
        ],
        'summary' => '', // Oculta el resumen de la cantidad de elementos
        'emptyText' => 'No se encontraron registros con efectividad media.',
        'options' => [
            'class' => 'tabla-tresRecientes', // Aplica la clase CSS personalizada al GridView
        ],
    ]); ?>
</div>

<div class="contenedor-titulo">
    <?= Html::a('VOLVER A EXPLORAR', ['builds/explorar'], ['class' => 'boton-vuelta-a-explorar']) ?>
</div>