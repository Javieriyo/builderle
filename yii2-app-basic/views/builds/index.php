<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Builds';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="builds-index">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Build', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= \yii\grid\GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'nombre',
            'descripcion',
            'fcreacion',
            'efectividad',
            'complejidad',
            // otros campos

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
