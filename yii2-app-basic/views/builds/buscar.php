<?php

use app\models\Builds;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\grid\LinkColumn;
use yii\web\Controller;
use yii\data\SqlDataProvider;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Buscar';
$this->params['breadcrumbs'][] = $this->title;
?>
<!--builds recientes-->
<div class="contenedor-titulo">
    <h1 class="titulo">BUILDS MÁS RECIENTES</h1>
</div>
<hr class="linea-divisoria-central">
<br>
<div class="contenedor-tabla-tresrecientes">
    <?= GridView::widget([
        'dataProvider' => $tresRecientes,
        'columns' => [
            [
                'attribute' => 'nombre',
                'label' => 'Nombre de la Build', 
                'format' => 'raw', 
                'value' => function ($model) {
                    return Html::a($model['nombre'], ['ver', 'nombre' => $model['nombre']]);
                },
            ],
            'fecha_creacion',
            'efectividad',
            'complejidad',
            'personaje_nombre',
        ],
        'tableOptions' => ['class' => 'tabla-tresRecientes'],
        'summary' => '', 
    ]); ?>
</div>
<br>
<div class="contenedor-titulo">
    <h1 class="titulo">TODAS LAS BUILDS</h1>
</div>
<hr class="linea-divisoria-central">
<br>
<div class="contenedor-tabla-tresrecientes">
    <?= GridView::widget([
        'dataProvider' => $todasBuilds,
        'columns' => [
            [
                'attribute' => 'nombre',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a($model['nombre'], ['ver', 'nombre' => $model['nombre']]);
                },
            ],
            'complejidad',
            'efectividad',
            'personaje_nombre',
        ],
        'tableOptions' => ['class' => 'tabla-tresRecientes'],
        'summary' => '', 
    ]); ?>
</div>
