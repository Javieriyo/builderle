<?php

use app\models\Builds;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\web\Controller;
use yii\data\SqlDataProvider;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Builds';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="contenedor-titulo">
    <h1 class="titulo">BUILDS PARA ENFRENTAR JEFES</h1>
</div>

<div class="contenedor-tabla-tresrecientes">
    <?= GridView::widget([
        'dataProvider' => $filtroMapas,
        'columns' => [
            [
                'attribute' => 'nombre',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a(Html::encode($model['nombre']), ['builds/ver', 'nombre' => $model['nombre']]);
                },
            ],
            [
                'attribute' => 'efectividad',
                'value' => function ($model) {
                    return empty($model['efectividad']) ? 'N/A' : $model['efectividad'];
                },
            ],
            [
                'attribute' => 'complejidad',
                'value' => function ($model) {
                    return empty($model['complejidad']) ? 'N/A' : $model['complejidad'];
                },
            ],
            [
                'attribute' => 'personaje_nombre',
                'value' => function ($model) {
                    return empty($model['personaje_nombre']) ? 'N/A' : $model['personaje_nombre'];
                },
            ],
            [
                'attribute' => 'casco_nombre',
                'value' => function ($model) {
                    return empty($model['casco_nombre']) ? 'N/A' : $model['casco_nombre'];
                },
            ],
            [
                'attribute' => 'collar_nombre',
                'value' => function ($model) {
                    return empty($model['collar_nombre']) ? 'N/A' : $model['collar_nombre'];
                },
            ],
            [
                'attribute' => 'arma_nombre',
                'value' => function ($model) {
                    return empty($model['arma_nombre']) ? 'N/A' : $model['arma_nombre'];
                },
            ],
            [
                'attribute' => 'armadura_nombre',
                'value' => function ($model) {
                    return empty($model['armadura_nombre']) ? 'N/A' : $model['armadura_nombre'];
                },
            ],
            [
                'attribute' => 'botas_nombre',
                'value' => function ($model) {
                    return empty($model['botas_nombre']) ? 'N/A' : $model['botas_nombre'];
                },
            ],
            [
                'attribute' => 'anillo_nombre',
                'value' => function ($model) {
                    return empty($model['anillo_nombre']) ? 'N/A' : $model['anillo_nombre'];
                },
            ],
        ],
        'tableOptions' => ['class' => 'tabla-tresRecientes'],
        'summary' => '', // Oculta el resumen
    ]); ?>
</div>

<div class="contenedor-titulo">
    <?= Html::a('VOLVER A EXPLORAR', ['builds/explorar'], ['class' => 'boton-vuelta-a-explorar']) ?>
</div>
