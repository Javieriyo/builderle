<?php

use app\models\Botas;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Botas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="botas-index">

<div class="contenedor-titulo">
    <h1 class="titulo">BOTAS</h1>
</div>

<div class="contenedor-tabla-tresrecientes">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            // Eliminando la columna de número de fila
            // 'class' => 'yii\grid\SerialColumn',

            'idbotas',
            'nombre',
            'porcentevasion',
            // Eliminando la columna de acciones
            // [
            //     'class' => ActionColumn::className(),
            //     'urlCreator' => function ($action, Botas $model, $key, $index, $column) {
            //         return Url::toRoute([$action, 'idbotas' => $model->idbotas]);
            //      }
            // ],
        ],
        'tableOptions' => ['class' => 'tabla-tresRecientes'],
        'summary' => '',
    ]); ?>
</div>
<div class="contenedor-titulo">
    <?= Html::a('VOLVER AL EQUIPO', ['builds/equipo'], ['class' => 'boton-vuelta-a-explorar']) ?>
</div>
</div>
