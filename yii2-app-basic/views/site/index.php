<?php
use yii\helpers\Html;
use yii\helpers\Url;
/** @var yii\web\View $this */

$this->title = 'BuilderLE';
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Mi Proyecto Yii2</title>

    <!-- Fuentes -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Forum&display=swap" rel="stylesheet">

    <!-- Agrega las hojas de estilo de Bootstrap -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

    <!-- Agrega tu propio archivo CSS si es necesario -->
    <link rel="stylesheet" href="<?= Url::to('@web/css/site.css') ?>">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;700&display=swap" rel="stylesheet">
    
    <!-- Incluye los scripts de Bootstrap y jQuery -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <!-- Agrega tus propios scripts si es necesario -->
</head>
<body>
<!--contenedor principal de la home-->
<!-- <div class="contenedor-principal">
</div> -->
    <!--titulo-->
    <div class="contenedor-titulo">
        <h1 class="titulo">LA MEJOR HERRAMIENTA PARA PROGRAMAR TU PROGRESO</h1>

    </div>
    <hr class="linea-divisoria-derecha">


    <!--buscar-->
    <div class="contenedor-buscar">
        <?= Html::img('@web/images/buscar.webp', ['class' => 'imagen-buscar']) ?>
        <div class="contenedor-buscar-texto">
            <p class="contenedor-buscar-texto-titulo">BUSCAR</p>
            <p class="contenedor-buscar-texto-parrafo">Busca dentro de todas las builds disponibles la que mas se asemeje a tu estilo de juego.</p>
            <?= Html::a('IR', ['builds/explorar'], ['class' => 'boton-buscar']) ?>
        </div>
    </div>
    <hr class="linea-divisoria-izquierda">

    <!-- compartir -->
    <div class="contenedor-compartir">
        <div class="contenedor-compartir-texto">
            <p class="contenedor-compartir-texto-titulo">CREAR</p>
            <p class="contenedor-compartir-texto-parrafo">Crea y customiza tu propia build y subela para que el resto de los jugadores puedan usarla.</p>
            <?= Html::a('IR', ['builds/create'], ['class' => 'boton-buscar']) ?>
        </div>
        <?= Html::img('@web/images/compartir.avif', ['class' => 'imagen-compartir']) ?>
    </div>
    <hr class="linea-divisoria-derecha">
    <!--Buscar-->
    <div class="contenedor-buscar">
        <?= Html::img('@web/images/PoEequipo.jpg', ['class' => 'imagen-buscar']) ?>
        <div class="contenedor-buscar-texto">
            <p class="contenedor-buscar-texto-titulo">EXPLORAR</p>
            <p class="contenedor-buscar-texto-parrafo">Filtra entre todas nuestras builds segun tus gustos y prioridades y encuentra la build perfecta para ti.</p>
            <?= Html::a('IR', ['builds/buscar'], ['class' => 'boton-buscar']) ?>
        </div>
    </div>
    <hr class="linea-divisoria-izquierda">

    <!--equipo-->
    <div class="contenedor-buscar">
        <div class="contenedor-buscar-texto">
            <p class="contenedor-buscar-texto-titulo">EQUIPO</p>
            <p class="contenedor-buscar-texto-parrafo">Explora el inventario de objetos de todo el juego y averigua que objetos te interesan.</p>
            <?= Html::a('IR', ['builds/equipo'], ['class' => 'boton-buscar']) ?>
        </div>
        <?= Html::img('@web/images/comparar.jpg', ['class' => 'imagen-compartir']) ?>
        </div>
    <hr class="linea-divisoria-derecha">

    <!--comparar-->
    <div class="contenedor-buscar">
        <?= Html::img('@web/images/trialmaster.jpg', ['class' => 'imagen-buscar']) ?>
        <div class="contenedor-buscar-texto">
            <p class="contenedor-buscar-texto-titulo">COMPARAR</p>
            <p class="contenedor-buscar-texto-parrafo">Si no estas seguro de que build utilizar comparala con las demas y elije la que mas te guste.</p>
            <?= Html::a('IR', ['builds/compare'], ['class' => 'boton-buscar']) ?>
        </div>
    </div>
    <hr class="linea-divisoria-izquierda">

</body>
</html>