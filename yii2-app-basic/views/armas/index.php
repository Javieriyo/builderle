<?php

use app\models\Armas;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Armas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="armas-index">
<div class="contenedor-titulo">
    <h1 class="titulo">ARMAS</h1>
</div>
<div class="contenedor-tabla-tresrecientes">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            // Eliminar la columna de número de fila
            // ['class' => 'yii\grid\SerialColumn'],

            'idarma',
            'nombre',
            'valoratk',
            'porcentfisicoatk',
            'porcentelementalatk',
            // Eliminar la columna de acciones
            // [
            //     'class' => ActionColumn::className(),
            //     'urlCreator' => function ($action, Armas $model, $key, $index, $column) {
            //         return Url::toRoute([$action, 'idarma' => $model->idarma]);
            //      }
            // ],
        ],
        'tableOptions' => ['class' => 'tabla-tresRecientes'],
        'summary' => '',
    ]); ?>
</div>
<div class="contenedor-titulo">
    <?= Html::a('VOLVER AL EQUIPO', ['builds/equipo'], ['class' => 'boton-vuelta-a-explorar']) ?>
</div>
</div>
