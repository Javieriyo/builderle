<?php

use app\models\Cascos;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Cascos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cascos-index">

<div class="contenedor-titulo">
    <h1 class="titulo">CASCOS</h1>
</div>

<div class="contenedor-tabla-tresrecientes">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            // Eliminar la columna de número de fila
            // ['class' => 'yii\grid\SerialColumn'],

            'idcasco',
            'nombre',
            'vidaextra',
            'regen',
            // Eliminar la columna de acciones
            // [
            //     'class' => ActionColumn::className(),
            //     'urlCreator' => function ($action, Cascos $model, $key, $index, $column) {
            //         return Url::toRoute([$action, 'idcasco' => $model->idcasco]);
            //      }
            // ],
        ],
        'tableOptions' => ['class' => 'tabla-tresRecientes'],
        'summary' => '',
    ]); ?>
</div>
<div class="contenedor-titulo">
    <?= Html::a('VOLVER AL EQUIPO', ['builds/equipo'], ['class' => 'boton-vuelta-a-explorar']) ?>
</div>
</div>
