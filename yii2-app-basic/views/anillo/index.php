<?php

use app\models\Anillos;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\grid\ActionColumn;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Anillos';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="anillos-index">
    <div class="contenedor-titulo">
        <h1 class="titulo">ANILLOS</h1>
    </div>

    <div class="contenedor-tabla-tresrecientes">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                'idanillo',
                'nombre',
                'probcrit',
                [
                    'class' => ActionColumn::className(),
                    'visibleButtons' => [
                        'view' => false,
                        'update' => false,
                        'delete' => false,
                    ],
                    'urlCreator' => function ($action, Anillos $model, $key, $index, $column) {
                        return Url::toRoute([$action, 'idanillo' => $model->idanillo]);
                    }
                ],
            ],
            'tableOptions' => ['class' => 'tabla-tresRecientes'],
            'summary' => '',
        ]); ?>
    </div>

    <div class="contenedor-titulo">
        <?= Html::a('VOLVER AL EQUIPO', ['builds/equipo'], ['class' => 'boton-vuelta-a-explorar']) ?>
    </div>
</div>
